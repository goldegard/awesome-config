# Awesome-Config
This is a simple project to have a fast/ready-to-use awesome window manager configuration.

### Usage
Insall awesome with `sudo apt-get install awesome awesome-extra`, then clone a copy of this repository in your `~/.config` folder:
```
cd ~/.config
git clone --recursive https://gitlab.com/goldegard/awesome-config.git
mv awesome-config awesome
```